﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from '../_models';
import { environment } from 'src/environments/environment';


@Injectable({ providedIn: 'root' })
export class UserService {    

    private baseUrl: String;

    constructor(private http: HttpClient) { 
        this.baseUrl = environment.baseUrl;
    }

    getAll() {
        return this.http.get<User[]>(this.baseUrl + `/users`);
    }

    register(user: User):any {
        return this.http.post<any>(this.baseUrl + `/signup`, user)
            .pipe(map(data =>{
                
                localStorage.setItem('currentUser', JSON.stringify(data.sessionToken));
                return data;
            }));
    }

    delete(id: number) {
        return this.http.delete(this.baseUrl + `/users/${id}`);
    }
}