﻿export class User {
    id: number;
    userName: string;
    password: string;
    firstName: string;
    lastName: string;
    location: string;
    companyName: string;
    companyurl: string;
    sessionToken: string;
}