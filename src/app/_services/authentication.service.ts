﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../_models';
import { environment } from 'src/environments/environment';



@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    public userName: String;
    private baseUrl: String;
    constructor(private http: HttpClient) {
        this.baseUrl = environment.baseUrl;
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        
        return this.currentUserSubject.value;
    }

    login(username, password) {
        console.log(this.baseUrl +`/login`)     
           
        return this.http.post<any>(this.baseUrl +`/login`, { username, password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                
                localStorage.setItem('currentUser', JSON.stringify(user.sessionToken));
                this.userName = user.userName;
                
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        
        localStorage.removeItem('currentUser');
        this.http.post<any>(this.baseUrl + `/logout?username=${this.userName}`, null)
            .pipe()
            .subscribe();
        this.currentUserSubject.next(null);
    }
}